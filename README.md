# DUDraw2

This is DUDraw2, an improved version of DUDraw for use with introductory Computer Science courses at the University of Denver.

DUDraw2 is a fork of [DUDraw](https://git.cs.du.edu/super-serious-software/dudraw/), which is a fork of [Standard Draw, Draw, and DrawListener from the Princeton Standard Libraries](https://introcs.cs.princeton.edu/java/stdlib/). 

DUDraw2 has several convenience features over DUDraw/StdDraw. Highlights include:
- Combined methods for filled vs. outlined shapes: use the `filled` flag to change.
- Overloaded methods for drawing shapes with colors: no longer neccessary to call `setPenColor()` before each shape of a different color.

Released under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.html)

## Getting Started 

Updated documentation will be available soon.

If you just want the jar file, click [here](https://www.bendossett.com/DUDraw2.jar)
