"""
stddraw.py

The stddraw module defines functions that allow the user to create a
drawing.  A drawing appears on the canvas.  The canvas appears
in the window.  As a convenience, the module also imports the
commonly used Color objects defined in the color module.
"""

import time
import os
import sys
import color
import math
import string


os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = 'hide'
import pygame
import pygame.gfxdraw
import pygame.font

if sys.hexversion < 0x03000000:
    import Tkinter
    import tkMessageBox
    import tkFileDialog
else:
    import tkinter as Tkinter
    import tkinter.messagebox as tkMessageBox
    import tkinter.filedialog as tkFileDialog

# -----------------------------------------------------------------------

# Define colors so clients need not import the color module.
from color import *

# -----------------------------------------------------------------------

# Default Sizes and Values

_BORDER: float = 0.0
_DEFAULT_XMIN: float = 0.0
_DEFAULT_XMAX: float = 1.0
_DEFAULT_YMIN: float = 0.0
_DEFAULT_YMAX: float = 1.0
_DEFAULT_CANVAS_SIZE: int = 512
_DEFAULT_PEN_RADIUS: float = 1.0  # 1.0 should correspond to a radius of 1 pixel on the canvas.
_DEFAULT_PEN_COLOR: Color = BLACK

_DEFAULT_FONT_FAMILY: str = 'Helvetica'
_DEFAULT_FONT_SIZE: float = 12

_xmin: float = None
_ymin: float = None
_xmax: float = None
_ymax: float = None

_fontFamily: str = _DEFAULT_FONT_FAMILY
_fontSize: float = _DEFAULT_FONT_SIZE

_canvasWidth: float = float(_DEFAULT_CANVAS_SIZE)
_canvasHeight: float = float(_DEFAULT_CANVAS_SIZE)
_penRadius: float = None
_penColor: Color = _DEFAULT_PEN_COLOR
_keysTyped: list[str] = []

# Has the window been created?
_windowCreated: bool = False

# -----------------------------------------------------------------------
# Begin added by Alan J. Broder
# -----------------------------------------------------------------------

# Keep track of mouse status

# Has the mouse been left-clicked since the last time we checked?
_mousePressed: bool = False

# The position of the mouse as of the most recent mouse click
_mousePos: bool = None


# -----------------------------------------------------------------------
# End added by Alan J. Broder
# -----------------------------------------------------------------------

def _pygameColor(c: Color):
    """
    Convert c, an object of type color.Color, to an equivalent object
    of type pygame.Color.  Return the result.
    """
    r = c.getRed()
    g = c.getGreen()
    b = c.getBlue()
    return pygame.Color(r, g, b)


# -----------------------------------------------------------------------

# Private functions to scale and factor X and Y values.

def _scaleX(x: float) -> float:
    return _canvasWidth * (x - _xmin) / (_xmax - _xmin)

def _scaleY(y: float) -> float:
    return _canvasHeight * (_ymax - y) / (_ymax - _ymin)

def _factorX(w: float) -> float:
    return w * _canvasWidth / abs(_xmax - _xmin)

def _factorY(h: float) -> float:
    return h * _canvasHeight / abs(_ymax - _ymin)


# -----------------------------------------------------------------------
# Begin added by Alan J. Broder
# -----------------------------------------------------------------------

def _userX(x: float) -> float:
    return _xmin + x * (_xmax - _xmin) / _canvasWidth


def _userY(y: float) -> float:
    return _ymax - y * (_ymax - _ymin) / _canvasHeight


# -----------------------------------------------------------------------
# End added by Alan J. Broder
# -----------------------------------------------------------------------

def setCanvasSize(w: int=_DEFAULT_CANVAS_SIZE, h: int=_DEFAULT_CANVAS_SIZE):
    """
    Set the size of the canvas to w pixels wide and h pixels high.
    Calling this function is optional. If you call it, you must do
    so before calling any drawing function.
    """
    global _background
    global _surface
    global _canvasWidth
    global _canvasHeight
    global _windowCreated

    if _windowCreated:
      raise Exception('The stddraw window already was created')

    if (w < 1) or (h < 1):
      raise Exception('width and height must be positive')

    _canvasWidth = w
    _canvasHeight = h
    _background = pygame.display.set_mode([w, h])
    pygame.display.set_caption('DUDraw window (r-click to save)')
    _surface = pygame.Surface((w, h))
    _surface.fill(_pygameColor(WHITE))
    _windowCreated = True

def getCanvasWidth() -> float:
    """
    Return the width of the current canvas
    """
    return _canvasWidth

def getCanvasHeight() -> float:
    """
    Return the height of the current canvas
    """
    return _canvasHeight

def getPixelColor(x: float, y: float) -> Color:
    """
    Return the color of the pixel at the given user coordinates
    """
    _ensureWindowCreated()
    return _surface.get_at((int(_scaleX(x)), int(_scaleY(y))))

def setXScale(min: float=_DEFAULT_XMIN, max: float=_DEFAULT_XMAX):
    """
    Set the x-scale of the canvas such that the minimum x value
    is min and the maximum x value is max.
    """
    global _xmin
    global _xmax
    min = float(min)
    max = float(max)
    if min >= max:
      raise Exception('min must be less than max')
    size = max - min
    _xmin = min - _BORDER * size
    _xmax = max + _BORDER * size


def setYScale(min: float=_DEFAULT_YMIN, max: float=_DEFAULT_YMAX):
    """
    Set the y-scale of the canvas such that the minimum y value
    is min and the maximum y value is max.
    """
    global _ymin
    global _ymax
    min = float(min)
    max = float(max)
    if min >= max:
      raise Exception('min must be less than max')
    size = max - min
    _ymin = min - _BORDER * size
    _ymax = max + _BORDER * size


def setScale():
    setXScale()
    setYScale()


def setScale(min: float, max: float):
    setXScale(min, max)
    setYScale(min, max)

def setPenRadius(r: float=_DEFAULT_PEN_RADIUS):
    """
    Set the pen radius to r, thus affecting the subsequent drawing
    of points and lines. If r is 0.0, then points will be drawn with
    the minimum possible radius and lines with the minimum possible
    width.
    """
    global _penRadius
    r = float(r)
    if r < 0.0:
        raise Exception('Argument to setPenRadius() must be non-negative')
    # _penRadius = r * float(_DEFAULT_CANVAS_SIZE)
    _penRadius = r


def setPenColor(c: Color=_DEFAULT_PEN_COLOR):
    """
    Set the pen color to c, where c is an object of class color.Color.
    c defaults to stddraw.BLACK.
    """
    global _penColor
    _penColor = c


def setPenColorRGB(r: int=255, g: int=255, b: int=255):
    """
    Set the pen color to c, where c is an object of class color.Color.
    c defaults to stddraw.BLACK.
    """
    setPenColor(color(r, g, b))

def setPenColorHex(hex: str):
    """
    Set the pen color using a hex string
    """
    setPenColor(color(hex=hex))


def getPenColor() -> Color:
    """
    Returns the value of _penColor as an object of class color.Color.
    """
    return _penColor


def setFontFamily(f: str=_DEFAULT_FONT_FAMILY):
    """
    Set the font family to f (e.g. 'Helvetica' or 'Courier').
    """
    global _fontFamily
    _fontFamily = f


def setFontSize(s: float=_DEFAULT_FONT_SIZE):
    """
    Set the font size to s (e.g. 12 or 16).
    """
    global _fontSize
    _fontSize = s


# -----------------------------------------------------------------------

def _ensureWindowCreated():
  global _windowCreated
  if not _windowCreated:
    setCanvasSize()


# -----------------------------------------------------------------------

# Functions to draw shapes, text, and images on the background canvas.

def _pixel(x: float, y: float):
    """
    Draw on the background canvas a pixel at (x, y).
    """
    _ensureWindowCreated()
    xs = _scaleX(x)
    xy = _scaleY(y)
    pygame.gfxdraw.pixel(
        _surface,
        int(round(xs)),
        int(round(xy)),
        _pygameColor(_penColor))


def point(x: float, y: float):
    """
    Draw on the background canvas a point at (x, y).
    """
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    # If the radius is too small, then simply draw a pixel.
    if _penRadius <= 1.0:
        _pixel(x, y)
    else:
        xs = _scaleX(x)
        ys = _scaleY(y)
        pygame.draw.ellipse(
            _surface,
            _pygameColor(_penColor),
            pygame.Rect(
                xs - _penRadius,
                ys - _penRadius,
                _penRadius * 2.0,
                _penRadius * 2.0),
            0)


def _thickLine(x0: float, y0: float, x1: float, y1: float, r: float):
    """
    Draw on the background canvas a line from (x0, y0) to (x1, y1).
    Draw the line with a pen whose radius is r.
    """
    xs0 = _scaleX(x0)
    ys0 = _scaleY(y0)
    xs1 = _scaleX(x1)
    ys1 = _scaleY(y1)
    if (abs(xs0 - xs1) < 1.0) and (abs(ys0 - ys1) < 1.0):
        filledCircle(x0, y0, r)
        return
    xMid = (x0 + x1) / 2
    yMid = (y0 + y1) / 2
    _thickLine(x0, y0, xMid, yMid, r)
    _thickLine(xMid, yMid, x1, y1, r)


def line(x0: float, y0: float, x1: float, y1: float):
    """
    Draw on the background canvas a line from (x0, y0) to (x1, y1).
    """

    THICK_LINE_CUTOFF = 3  # pixels

    _ensureWindowCreated()

    x0 = float(x0)
    y0 = float(y0)
    x1 = float(x1)
    y1 = float(y1)

    lineWidth = _penRadius * 2.0
    if lineWidth == 0.0: lineWidth = 1.0
    if lineWidth < THICK_LINE_CUTOFF:
        x0s = _scaleX(x0)
        y0s = _scaleY(y0)
        x1s = _scaleX(x1)
        y1s = _scaleY(y1)
        pygame.draw.line(
            _surface,
            _pygameColor(_penColor),
            (x0s, y0s),
            (x1s, y1s),
            int(round(lineWidth)))
    else:
        _thickLine(x0, y0, x1, y1, _penRadius / _DEFAULT_CANVAS_SIZE)


def ellipseHelper(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, filled: bool):
    """
    Draw on the background canvas a circle of radius r centered on
    (x, y).
    """
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    semiMajorAxis = float(semiMajorAxis)
    semiMinorAxis = float(semiMinorAxis)
    ws = _factorX(2.0 * semiMajorAxis)
    hs = _factorY(2.0 * semiMinorAxis)
    # If the radius is too small, then simply draw a pixel.
    if (ws <= 1.0) and (hs <= 1.0):
        _pixel(x, y)
    else:
        xs = _scaleX(x)
        ys = _scaleY(y)
        pygame.draw.ellipse(
            _surface,
            _pygameColor(_penColor),
            pygame.Rect(xs - ws / 2.0, ys - hs / 2.0, ws, hs),
            0 if filled else int(round(_penRadius)))


def circle(x: float, y: float, r: float):
    """
    Draw on the background canvas a circle of radius r centered on
    (x, y).
    """
    ellipseHelper(x, y, r, r, False)


def filledCircle(x: float, y: float, r: float):
    """
    Draw on the background canvas a filled circle of radius r
    centered on (x, y).
    """
    ellipseHelper(x, y, r, r, True)


def ellipse(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float):
    """
    Draw on the background canvas a circle of radius r centered on
    (x, y).
    """
    ellipseHelper(x, y, semiMajorAxis, semiMinorAxis, False)


def filledEllipse(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float):
    """
    Draw on the background canvas a circle of radius r centered on
    (x, y).
    """
    ellipseHelper(x, y, semiMajorAxis, semiMinorAxis, True)


def rectangleHelper(x: float, y: float, halfWidth: float, halfHeight: float, filled: bool):
    global _surface
    _ensureWindowCreated()
    x = float(x) - float(halfWidth)
    y = float(y) - float(halfHeight)
    halfWidth = 2 * float(halfWidth)
    halfHeight = 2 * float(halfHeight)
    ws = _factorX(halfWidth)
    hs = _factorY(halfHeight)
    # If the rectangle is too small, then simply draw a pixel.
    if (ws <= 1.0) and (hs <= 1.0):
        _pixel(x, y)
    else:
        xs = _scaleX(x)
        ys = _scaleY(y)
        pygame.draw.rect(
            _surface,
            _pygameColor(_penColor),
            pygame.Rect(xs, ys - hs, ws, hs),
            0 if filled else int(round(_penRadius)))


def rectangle(x: float, y: float, halfWidth: float, halfHeight: float):
    """
    Draw on the background canvas a rectangle of width (2 * halfWidth
    and height (2 * halfHeight) centered at point (x, y).
    """
    rectangleHelper(x, y, halfWidth, halfHeight, False)


def filledRectangle(x: float, y: float, halfWidth: float, halfHeight: float):
    """
    Draw on the background canvas a rectangle of width (2 * halfWidth
    and height (2 * halfHeight) centered at point (x, y).
    """
    rectangleHelper(x, y, halfWidth, halfHeight, True)


def square(x, y, r):
    """
    Draw on the background canvas a square whose sides are of length
    2r, centered on (x, y).
    """
    rectangleHelper(x - r, y - r, 2.0 * r, 2.0 * r, False)


def filledSquare(x, y, r):
    """
    Draw on the background canvas a filled square whose sides are of
    length 2r, centered on (x, y).
    """
    rectangleHelper(x - r, y - r, 2.0 * r, 2.0 * r, True)


def polygonHelper(x: list[float], y: list[float], filled: bool):
    global _surface
    _ensureWindowCreated()
    # Scale X and Y values.
    xScaled = [_scaleX(float(xi)) for xi in x]
    yScaled = [_scaleY(float(yi)) for yi in y]
    points = [(xScaled[i], yScaled[i]) for i in range(len(x))]
    points.append((xScaled[0], yScaled[0]))
    pygame.draw.polygon(
        _surface,
        _pygameColor(_penColor),
        points,
        0 if filled else int(round(_penRadius)))

def polygon(x: list[float], y: list[float]):
    """
    Draw on the background canvas a polygon with coordinates
    (x[i], y[i]).
    """
    polygonHelper(x, y, False)


def filledPolygon(x: list[float], y: list[float]):
    """
    Draw on the background canvas a filled polygon with coordinates
    (x[i], y[i]).
    """
    polygonHelper(x, y, True)


def triangle(x0: float, y0: float, x1: float, y1: float, x2: float, y2: float):
    """
    :param x0:
    :param y0:
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    """
    polygonHelper([x0, x1, x2], [y0, y1, y2], False)


def filledTriangle(x0: float, y0: float, x1: float, y1: float, x2: float, y2: float):
    """
    :param x0:
    :param y0:
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    """
    polygonHelper([x0, x1, x2], [y0, y1, y2], True)


def quadrilateral(x0: float, y0: float, x1: float, y1: float, x2: float, y2: float, x3: float, y3: float):
    """
    :param x0:
    :param y0:
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    :param x3:
    :param y3:
    """
    polygonHelper([x0, x1, x2, x3], [y0, y1, y2, y3], False)


def filledQuadrilateral(x0: float, y0: float, x1: float, y1: float, x2: float, y2: float, x3: float, y3: float):
    """
    :param x0:
    :param y0:
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    :param x3:
    :param y3:
    """
    polygonHelper([x0, x1, x2, x3], [y0, y1, y2, y3], True)


def arcHelper(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, angle1: float, angle2: float):
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    semiMajorAxis = float(semiMajorAxis)
    semiMinorAxis = float(semiMinorAxis)
    angle1 = float(angle1)
    angle2 = float(angle2)
    while (angle2 - angle1) < 0:
        angle2 += 360
    circlePoints = 4 * (_factorX(semiMajorAxis) + _factorY(semiMinorAxis))
    numPoints = circlePoints * ((angle2 - angle1)/360)
    for i in range(0, int(numPoints)):
        angleIn = angle1 + (i * 360/circlePoints)
        angleIn = angleIn * math.pi / 180
        x0 = (math.cos(angleIn) * semiMajorAxis) + x
        y0 = (math.sin(angleIn) * semiMinorAxis) + y
        angleOut = angle1 + ((i+1) * 360/circlePoints)
        angleOut = angleOut * math.pi / 180
        x1 = (math.cos(angleOut) * semiMajorAxis) + x
        y1 = (math.sin(angleOut) * semiMinorAxis) + y
        line(x0, y0, x1, y1)


def arc(x: float, y: float, r: float, angle1: float, angle2: float):
    """
    :param x: 
    :param y: 
    :param r: 
    :param angle1: 
    :param angle2: 
    :return: 
    """
    arcHelper(x, y, r, r, angle1, angle2)


def ellipticalArc(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, angle1: float, angle2: float):
    """
    :param x:
    :param y:
    :param semiMajorAxis:
    :param semiMinorAxis:
    :param angle1:
    :param angle2:
    :return:
    """
    arcHelper(x, y, semiMajorAxis, semiMinorAxis, angle1, angle2)


def sectorHelper(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, angle1: float, angle2: float, filled: bool):
    global _surface
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    semiMajorAxis = float(semiMajorAxis)
    semiMinorAxis = float(semiMinorAxis)
    angle1 = float(angle1)
    angle2 = float(angle2)
    while (angle2 - angle1) < 0:
        angle2 += 360
    circlePoints = 4 * (_factorX(semiMajorAxis) + _factorY(semiMinorAxis))
    numPoints = circlePoints * ((angle2 - angle1) / 360)
    xvals = [x]
    yvals = [y]
    for i in range(0, int(numPoints) + 1):
        angle = angle1 + (i * 360 / circlePoints)
        angle = angle * math.pi / 180
        x0 = (math.cos(angle) * semiMajorAxis) + x
        y0 = (math.sin(angle) * semiMinorAxis) + y
        xvals.append(x0)
        yvals.append(y0)
    xvals.append((math.cos(angle2 * math.pi / 180) * semiMajorAxis) + x)
    yvals.append((math.sin(angle2 * math.pi / 180) * semiMinorAxis) + y)
    xvals.append(x)
    yvals.append(y)
    points = [(_scaleX(xvals[i]), _scaleY(yvals[i])) for i in range(len(xvals))]
    pygame.draw.polygon(
        _surface,
        _pygameColor(_penColor),
        points,
        0 if filled else int(round(_penRadius)))


def sector(x: float, y: float, r: float, angle1: float, angle2: float):
    """
    :param x:
    :param y:
    :param r:
    :param angle1:
    :param angle2:
    :return:
    """
    sectorHelper(x, y, r, r, angle1, angle2, False)


def filledSector(x: float, y: float, r: float, angle1: float, angle2: float):
    """
    :param x:
    :param y:
    :param r:
    :param angle1:
    :param angle2:
    :return:
    """
    sectorHelper(x, y, r, r, angle1, angle2, True)


def ellipticalSector(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, angle1: float, angle2: float):
    """
    :param x:
    :param y:
    :param semiMajorAxis:
    :param semiMinorAxis:
    :param angle1:
    :param angle2:
    :return:
    """
    sectorHelper(x, y, semiMajorAxis, semiMinorAxis, angle1, angle2, False)


def filledEllipticalSector(x: float, y: float, semiMajorAxis: float, semiMinorAxis: float, angle1: float, angle2: float):
    """
    :param x:
    :param y:
    :param semiMajorAxis:
    :param semiMinorAxis:
    :param angle1:
    :param angle2:
    :return:
    """
    sectorHelper(x, y, semiMajorAxis, semiMinorAxis, angle1, angle2, True)


def annulus(x: float, y: float, r1: float, r2: float):
    _ensureWindowCreated()
    ellipseHelper(x, y, r1, r1, False)
    ellipseHelper(x, y, r2, r2, False)


def filledAnnulus(x: float, y: float, r1: float, r2: float):
    global _surface
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    r1 = float(r1)
    r2 = float(r2)

    circle1Points = 4 * (_factorX(r1) + _factorY(r1))
    circle2Points = 4 * (_factorX(r2) + _factorY(r2))
    xvals = []
    yvals = []
    for i in range(0, int(circle1Points) + 1):
        angle = i * 360 / circle1Points
        print(angle)
        angle = angle * math.pi / 180
        x0 = (math.cos(angle) * r1) + x
        y0 = (math.sin(angle) * r1) + y
        xvals.append(x0)
        yvals.append(y0)
    xvals.append(x+ r1)
    yvals.append(y)
    xvals.append(x + r2)
    yvals.append(y)
    for i in (range(int(circle2Points), -1, -1)):
        angle = i * 360 / circle2Points
        print(angle)
        angle = angle * math.pi / 180
        x0 = (math.cos(angle) * r2) + x
        y0 = (math.sin(angle) * r2) + y
        xvals.append(x0)
        yvals.append(y0)
    xvals.append(x + r2)
    yvals.append(y)
    xvals.append(x+ r1)
    yvals.append(y)
    points = [(_scaleX(xvals[i]), _scaleY(yvals[i])) for i in range(len(xvals))]
    pygame.draw.polygon(
        _surface,
        _pygameColor(_penColor),
        points,
        0)


def text(x: float, y: float, s: str):
    """
    Draw string s on the background canvas centered at (x, y).
    """
    _ensureWindowCreated()
    x = float(x)
    y = float(y)
    xs = _scaleX(x)
    ys = _scaleY(y)
    font = pygame.font.SysFont(_fontFamily, _fontSize)
    text = font.render(s, 1, _pygameColor(_penColor))
    textpos = text.get_rect(center=(xs, ys))
    _surface.blit(text, textpos)


def picture(pic, x: float=None, y: float=None):
    """
    Draw pic on the background canvas centered at (x, y).  pic is an
    object of class picture.Picture. x and y default to the midpoint
    of the background canvas.
    """
    global _surface
    _ensureWindowCreated()
    # By default, draw pic at the middle of the surface.
    if x is None:
        x = (_xmax + _xmin) / 2.0
    if y is None:
        y = (_ymax + _ymin) / 2.0
    x = float(x)
    y = float(y)
    xs = _scaleX(x)
    ys = _scaleY(y)
    ws = pic.width()
    hs = pic.height()
    picSurface = pic._surface  # violates encapsulation
    _surface.blit(picSurface, [xs - ws / 2.0, ys - hs / 2.0, ws, hs])


def clear(c: Color=WHITE):
    """
    Clear the background canvas to color c, where c is an
    object of class color.Color. c defaults to stddraw.WHITE.
    """
    _ensureWindowCreated()
    _surface.fill(_pygameColor(c))


def clearRGB(r=255, g=255, b=255):
    """
    Clear the background canvas to color c, where c is an
    object of class color.Color. c defaults to stddraw.WHITE.
    """
    c = color(r, g, b)
    _ensureWindowCreated()
    _surface.fill(_pygameColor(c))


def save(f):
    """
    Save the window canvas to file f.
    """
    _ensureWindowCreated()

    # if sys.hexversion >= 0x03000000:
    #    # Hack because Pygame without full image support
    #    # can handle only .bmp files.
    #    bmpFileName = f + '.bmp'
    #    pygame.image.save(_surface, bmpFileName)
    #    os.system('convert ' + bmpFileName + ' ' + f)
    #    os.system('rm ' + bmpFileName)
    # else:
    #    pygame.image.save(_surface, f)

    pygame.image.save(_surface, f)


# -----------------------------------------------------------------------

def _show():
    """
    Copy the background canvas to the window canvas.
    """
    _background.blit(_surface, (0, 0))
    pygame.display.flip()
    _checkForEvents()


def _showAndWaitForever():
    """
    Copy the background canvas to the window canvas. Then wait
    forever, that is, until the user closes the stddraw window.
    """
    _ensureWindowCreated()
    _show()
    QUANTUM = .1
    while True:
        time.sleep(QUANTUM)
        _checkForEvents()


def show(msec=float('inf')):
    """
    Copy the background canvas to the window canvas, and
    then wait for msec milliseconds. msec defaults to infinity.
    """
    if msec == float('inf'):
        _showAndWaitForever()

    _ensureWindowCreated()
    _show()
    _checkForEvents()

    # Sleep for the required time, but check for events every
    # QUANTUM seconds.
    QUANTUM = .1
    sec = msec / 1000.0
    if sec < QUANTUM:
        time.sleep(sec)
        return
    secondsWaited = 0.0
    while secondsWaited < sec:
        time.sleep(QUANTUM)
        secondsWaited += QUANTUM
        _checkForEvents()


# -----------------------------------------------------------------------

def _saveToFile():
    """
    Display a dialog box that asks the user for a file name.  Save the
    drawing to the specified file.  Display a confirmation dialog box
    if successful, and an error dialog box otherwise.  The dialog boxes
    are displayed using Tkinter, which (on some computers) is
    incompatible with Pygame. So the dialog boxes must be displayed
    from child processes.
    """
    import subprocess
    _ensureWindowCreated()

    stddrawPath = os.path.realpath(__file__)

    childProcess = subprocess.Popen(
        [sys.executable, stddrawPath, 'getFileName'],
        stdout=subprocess.PIPE)
    so, se = childProcess.communicate()
    fileName = so.strip()

    if sys.hexversion >= 0x03000000:
        fileName = fileName.decode('utf-8')

    if fileName == '':
        return

    if not fileName.endswith(('.jpg', '.png')):
        childProcess = subprocess.Popen(
            [sys.executable, stddrawPath, 'reportFileSaveError',
             'File name must end with ".jpg" or ".png".'])
        return

    try:
        save(fileName)
        childProcess = subprocess.Popen(
            [sys.executable, stddrawPath, 'confirmFileSave'])
    except pygame.error as e:
        childProcess = subprocess.Popen(
            [sys.executable, stddrawPath, 'reportFileSaveError', str(e)])


def _checkForEvents():
    """
    Check if any new event has occured (such as a key typed or button
    pressed).  If a key has been typed, then put that key in a queue.
    """
    global _surface
    global _keysTyped

    # -------------------------------------------------------------------
    # Begin added by Alan J. Broder
    # -------------------------------------------------------------------
    global _mousePos
    global _mousePressed
    # -------------------------------------------------------------------
    # End added by Alan J. Broder
    # -------------------------------------------------------------------

    _ensureWindowCreated()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            _keysTyped = [event.unicode] + _keysTyped
        elif (event.type == pygame.MOUSEBUTTONUP) and \
                (event.button == 3):
            _saveToFile()

        # ---------------------------------------------------------------
        # Begin added by Alan J. Broder
        # ---------------------------------------------------------------
        # Every time the mouse button is pressed, remember
        # the mouse position as of that press.
        elif (event.type == pygame.MOUSEBUTTONDOWN) and \
                (event.button == 1):
            _mousePressed = True
            _mousePos = event.pos
            # ---------------------------------------------------------------
        # End added by Alan J. Broder
        # ---------------------------------------------------------------


# -----------------------------------------------------------------------

# Functions for retrieving keys

def hasNextKeyTyped() -> bool:
    """
    Return True if the queue of keys the user typed is not empty.
    Otherwise return False.
    """
    global _keysTyped
    return _keysTyped != []


def nextKeyTyped() -> str:
    """
    Remove the first key from the queue of keys that the the user typed,
    and return that key.
    """
    global _keysTyped
    return _keysTyped.pop()


# -----------------------------------------------------------------------
# Begin added by Alan J. Broder
# -----------------------------------------------------------------------

# Functions for dealing with mouse clicks 

def mousePressed() -> bool:
    """
    Return True if the mouse has been left-clicked since the 
    last time mousePressed was called, and False otherwise.
    """
    global _mousePressed
    if _mousePressed:
        _mousePressed = False
        return True
    return False


def mouseX() -> float:
    """
    Return the x coordinate in user space of the location at
    which the mouse was most recently left-clicked. If a left-click
    hasn't happened yet, raise an exception, since mouseX() shouldn't
    be called until mousePressed() returns True.
    """
    global _mousePos
    if _mousePos:
        return _userX(_mousePos[0])
    raise Exception(
        "Can't determine mouse position if a click hasn't happened")


def mouseY() -> float:
    """
    Return the y coordinate in user space of the location at
    which the mouse was most recently left-clicked. If a left-click
    hasn't happened yet, raise an exception, since mouseY() shouldn't
    be called until mousePressed() returns True.
    """
    global _mousePos
    if _mousePos:
        return _userY(_mousePos[1])
    raise Exception(
        "Can't determine mouse position if a click hasn't happened")


# -----------------------------------------------------------------------
# End added by Alan J. Broder
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------

# Initialize the x scale, the y scale, and the pen radius.

setXScale()
setYScale()
setPenRadius()
pygame.font.init()


# -----------------------------------------------------------------------

# Functions for displaying Tkinter dialog boxes in child processes.

def _getFileName():
    """
    Display a dialog box that asks the user for a file name.
    """
    root = Tkinter.Tk()
    root.withdraw()
    reply = tkFileDialog.asksaveasfilename(initialdir='.')
    sys.stdout.write(reply)
    sys.stdout.flush()
    sys.exit()


def _confirmFileSave():
    """
    Display a dialog box that confirms a file save operation.
    """
    root = Tkinter.Tk()
    root.withdraw()
    tkMessageBox.showinfo(title='File Save Confirmation',
                          message='The drawing was saved to the file.')
    sys.exit()


def _reportFileSaveError(msg):
    """
    Display a dialog box that reports a msg.  msg is a string which
    describes an error in a file save operation.
    """
    root = Tkinter.Tk()
    root.withdraw()
    tkMessageBox.showerror(title='File Save Error', message=msg)
    sys.exit()


# -----------------------------------------------------------------------

def _regressionTest():
    """
    Perform regression testing.
    """

    clear()

    print(_canvasWidth, ", ", _canvasHeight)

    # setPenColor(MAGENTA)
    # setPenRadius(1)
    # line(.47, .25, .47, .75)
    # setPenRadius(2)
    # line(.5, .25, .5, .75)
    # setPenRadius(3)
    # line(.53, .25, .53, .75)
    # show(0.0)

    # setPenColor(CYAN)
    # setPenRadius(1)
    # line(.25, .47, .75, .47)
    # setPenRadius(2)
    # line(.25, .5, .75, .5)
    # setPenRadius(3)
    # line(.25, .53, .75, .53)
    # show(0.0)

    # setPenRadius(.5)
    # setPenColor(ORANGE)
    # point(0.5, 0.5)
    # show(0.0)
    #
    # setPenRadius(.25)
    # setPenColor(BLUE)
    # point(0.5, 0.5)
    # show(0.0)
    #
    # setPenRadius(.02)
    # setPenColor(RED)
    # point(0.25, 0.25)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(GREEN)
    # point(0.25, 0.25)
    # show(0.0)
    #
    # setPenRadius(0)
    # setPenColor(BLACK)
    # point(0.25, 0.25)
    # show(0.0)
    #
    # setPenRadius(.1)
    # setPenColor(RED)
    # point(0.75, 0.75)
    # show(0.0)
    #
    # setPenRadius(0)
    # setPenColor(CYAN)
    # for i in range(0, 100):
    #     point(i / 512.0, .5)
    #     point(.5, i / 512.0)
    # show(0.0)
    #
    # setPenRadius(0)
    # setPenColor(MAGENTA)
    # line(.1, .1, .3, .3)
    # line(.1, .2, .3, .2)
    # line(.2, .1, .2, .3)
    # show(0.0)
    #
    # setPenRadius(.05)
    # setPenColor(MAGENTA)
    # line(.7, .5, .8, .9)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(YELLOW)
    # circle(.75, .25, .2)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(YELLOW)
    # filledCircle(.75, .25, .1)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(PINK)
    # rectangle(.25, .75, .1, .2)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(PINK)
    # filledRectangle(.25, .75, .05, .1)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(DARK_RED)
    # square(.5, .5, .1)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(DARK_RED)
    # filledSquare(.5, .5, .05)
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(DARK_BLUE)
    # polygon([.4, .5, .6], [.7, .8, .7])
    # show(0.0)
    #
    # setPenRadius(.01)
    # setPenColor(DARK_GREEN)
    # setFontSize(24)
    # text(.2, .4, 'hello, world')
    # show(0.0)

    # triangle(0.1, 0.1, 0.3, 0.1, 0.2, 0.3)
    # quadrilateral(0.9, 0.9, 0.7, 0.9, 0.6, 0.7, 0.8, 0.7)
    # show(0.0)

    # ellipticalSector(0.8, 0.2, 0.1, 0.2, 220, 90)
    # filledEllipse(0.5, 0.5, 0.2, 0.1)
    # show(0.0)


    # import picture as p
    # pic = p.Picture('saveIcon.png')
    # picture(pic, .5, .85)
    # show(0.0)

    # Test handling of mouse and keyboard events.
    setPenColor(BLACK)
    import stdio
    stdio.writeln('Left click with the mouse or type a key')
    while True:
        if mousePressed():
            filledCircle(mouseX(), mouseY(), .02)
        if hasNextKeyTyped():
            stdio.write(nextKeyTyped())
        show(0.0)

    # Never get here.
    show()


# -----------------------------------------------------------------------

def _main():
    """
    Dispatch to a function that does regression testing, or to a
    dialog-box-handling function.
    """
    import sys
    if len(sys.argv) == 1:
        _regressionTest()
    elif sys.argv[1] == 'getFileName':
        _getFileName()
    elif sys.argv[1] == 'confirmFileSave':
        _confirmFileSave()
    elif sys.argv[1] == 'reportFileSaveError':
        _reportFileSaveError(sys.argv[2])


if __name__ == '__main__':
    _main()
